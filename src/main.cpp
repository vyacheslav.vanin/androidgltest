#include <vvv3d/vvv3d.hpp>

int main(int argc, char** argv) {
    vvv3d::Engine e(argc, argv);
    e.run();
}
